#include "image.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>


/* создаёт копию изображения, которая повёрнута на 90 градусов */
struct image rotate( struct image const source, long angle_deg){
    
    //printf("rotate starting...\n");
    struct image res;
    
    if(angle_deg == ZERO || check_valid_angle(angle_deg) == 0){
        res.width=source.width;
        res.height=source.height;
        res.data=source.data;
        return res;
    }
    struct pixel* p = malloc(sizeof(struct pixel)*source.width*source.height);
    if (p == NULL){
        res = (struct image){.width = 0, .height = 0, .data = 0};
        return res;
    }
    if(angle_deg == CW90 || angle_deg == CCW270){

        //printf("rotate cw 90\n");
        //clockwise
        for (int64_t row = 0; row < source.height; ++row){
            for (int64_t col = 0; col < source.width; ++col){
                struct pixel q = source.data[row*source.width+col];
                int64_t row2 = col;
                int64_t col2 = (int64_t)source.height - 1 - row;
                p[row2*source.height+col2] = q;
            }
        }
        res.width=source.height;
        res.height=source.width;
        res.data=p;
    }
    else if(angle_deg == CW180 || angle_deg == CCW180){
        //printf("rotate 180\n");
        for (int64_t row = 0; row < source.height; ++row){
            for (int64_t col = 0; col < source.width; ++col){
                struct pixel q = source.data[row*source.width+col];
                int64_t row2 = (int64_t)source.height - 1 - row;
                int64_t col2 = (int64_t)source.width - 1 - col;
                p[row2*source.width+col2] = q;
            }
        }
        res.width=source.width;
        res.height=source.height;
        res.data=p;
    }
    else {
        //printf("rotate ccw 90\n");
        for (int64_t row = 0; row < source.height; ++row){
            for (int64_t col = 0; col < source.width; ++col){
                struct pixel q = source.data[row*source.width+col];
                int64_t row2 = (int64_t)source.width - 1 - col;
                int64_t col2 = row;
                p[row2*source.height+col2] = q;
            }
        }
        res.width=source.height;
        res.height=source.width;
        res.data=p;
    }
    return res;
}

uint64_t count_stub_bytes(struct image const* p){
    return 4 - p->width * 3 % 4;
}

void clear(struct image* p){
    free(p->data);
    p->data = NULL;
    p->width = 0;
    p->height = 0;
}
int check_valid_angle(long d){
    if (d == ZERO || d == CW180 || d == CW270 || d == CW90 || d == CCW180 || d == CCW270 || d == CCW90){
        return 1;
    }else{
        return 0;
    }
}
