#include "bmp.h"
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>


void helpmsg(char* progname) {
  fprintf(stderr,
          "Usage: ./%s BMP_FILE_NAME1 BMP_FILE_NAME2\n", progname);
}

int main(int argc, char **argv) {
  if (argc != 4){
    helpmsg(argv[0]);
    return 1;
  }
  // error handling should be here
  FILE *f1 = fopen(argv[1], "rb");
  if (f1 == NULL){
    //fprintf(stderr, "Can`t open '%s'\n", argv[1]);
    return 1;
    }
  FILE *f2 = fopen(argv[2], "wb");
  if (f2 == NULL) {
    fclose(f1);
    return 1;
    //fprintf(stderr, "Can`t open '%s'\n", argv[2]);
  }
long angle_deg = strtol(argv[3], NULL, 10);
  //printf("angle_deg = %d\n", angle_deg);
  struct image img = {.data=NULL};
  enum read_status rs = from_bmp(f1, &img);  
  if (rs != READ_OK){
    //fprintf(stderr, "File read - error\n");
    return 1;
  }
  
  struct image img2 = rotate(img, angle_deg);
  if (img2.data == NULL){
    clear(&img);
    return 1;
  }
  enum write_status ws = to_bmp(f2, &img2);
  if (ws != WRITE_OK){
    //fprintf(stderr, "File write - error\n");
    return 1;
  }
  if (img.data == img2.data){
    clear(&img);
  }else{
    clear(&img);
    clear(&img2);
  }
  
  fclose(f1);
  fclose(f2);
  return 0;
}
