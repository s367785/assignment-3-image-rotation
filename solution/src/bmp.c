#include "bmp.h"

#include <stdio.h>
#include <stdlib.h>

int check_header(struct bmp_header* p){
    if (p->bfType !=  BMP_CODE|| p->bOffBits != BMP_HEADER_OFFSET || p->biBitCount != BMP_BIT_PER_PIXEL){
        return 0;
    }
    return 1;
}

enum read_status from_bmp( FILE* in, struct image* img ){
    struct bmp_header h;
    int64_t k = 0;
    k = (int)fread(&h, sizeof (struct bmp_header), 1, in);
    
    if (k != 1){
        return READ_INVALID_HEADER;
    }

    if (check_header(&h) == 0){
        return READ_INVALID_HEADER;
    }

    img -> width = h.biWidth;
    img -> height = h.biHeight;

    //printf("width = %llu  height = %llu\n", img->width, img->height);
    //clear(&img);
    free(img->data);
    img -> data = malloc(sizeof (struct pixel) * img -> width * img -> height);
    //uint64_t bytes_per_line = sizeof (struct pixel) * img -> width;
    
    uint16_t stub = count_stub_bytes(img);
    
    uint8_t buf[4];
    for (int64_t row = 0; row < img -> height; ++row){
        // read next line
        // first line - bottom of image        
        k = (int)fread(img->data+(img->height-1-row)*img->width, 1, img->width*3, in);
        if(k!=img->width*3){
            return READ_INVALID_BYTE;
        }
        if(stub!=0){
            k = (int)fread(buf, 1, stub, in);
            if(k!=stub) {
                return READ_INVALID_BYTE;
            }
        }
    }
    return READ_OK;
}

void fill_bmp_header(struct bmp_header* p, uint64_t w, uint64_t h, uint64_t stub){
    p->bfType = BMP_CODE;    
    p->bfileSize = BMP_HEADER_OFFSET + h * (w * 3 + stub);
    p->bfReserved = 0;
    p->bOffBits = BMP_HEADER_OFFSET;
    p->biSize = BMP_BITMAP_HEADER_OFFSET;
    p->biWidth = w;
    p->biHeight = h;
    p->biPlanes = 1;
    p->biBitCount = BMP_BIT_PER_PIXEL;
    p->biCompression = 0;
    p->biSizeImage = h * (w * 3 + stub);
    p->biXPelsPerMeter = BMP_DEFAULT_PIXEL_PER_METER;
    p->biYPelsPerMeter = BMP_DEFAULT_PIXEL_PER_METER;
    p->biClrUsed = 0;
    p->biClrImportant = 0;
}

enum write_status to_bmp( FILE* out, struct image const* img ){
    
    struct bmp_header h;
    uint64_t stub = count_stub_bytes(img);
    fill_bmp_header(&h, img->width, img->height, stub);
    int k = (int)fwrite(&h, sizeof h, 1, out);
    
    if (k != 1){
       return WRITE_ERROR;
    }
    uint8_t buf[4] = {0};
    for (int64_t i = 0; i < img -> height; ++i){
        //write current line of pixels
        k = (int)fwrite(img -> data + (img->height-1-i) * img -> width, 3, img -> width, out);        
        if (k != img -> width){
            return WRITE_ERROR;
        }
        if (stub != 0){
           k = (int)fwrite(buf, 1, stub, out);
            if (k != stub) {
                return WRITE_INVALID_BYTE;
            }
        }
    }
    return WRITE_OK;
}
