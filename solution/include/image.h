#pragma once
#include <inttypes.h>
#pragma pack(push, 1)
struct pixel{
    uint8_t b, g, r;
};
#pragma pack(pop)
struct image {
  uint64_t width, height;
  struct pixel* data;
};
struct image rotate( struct image const source, long angle_deg);
uint64_t count_stub_bytes(struct image const* p);
enum angle_degrees{
  ZERO = 0,
  CW90 = 90, 
  CW180 = 180, 
  CW270 = 270, 
  CCW90 = -90, 
  CCW180 = -180, 
  CCW270 = -270
};
void clear(struct image* p);
int check_valid_angle(long d);
