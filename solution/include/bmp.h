#pragma once
#include "image.h"
#include <inttypes.h>
#include <stdint.h>
#include <stdio.h>
#pragma pack(push, 1)
struct bmp_header 
{
        uint16_t bfType;
        uint32_t bfileSize;
        uint32_t bfReserved;
        uint32_t bOffBits;
        uint32_t biSize;
        uint32_t biWidth;
        uint32_t biHeight;
        uint16_t biPlanes;
        uint16_t biBitCount;
        uint32_t biCompression;
        uint32_t biSizeImage;
        uint32_t biXPelsPerMeter;
        uint32_t biYPelsPerMeter;
        uint32_t biClrUsed;
        uint32_t biClrImportant;
};
#pragma pack(pop)
/*  deserializer   */
enum read_status  {
  READ_OK = 0,
  READ_INVALID_SIGNATURE,
  READ_INVALID_BITS,
  READ_INVALID_HEADER,
  READ_INVALID_BYTE
  /* коды других ошибок  */
  };

enum read_status from_bmp( FILE* in, struct image* img );

enum bmp_const{
  BMP_CODE=0x4d42,
  BMP_HEADER_OFFSET=54, 
  BMP_BIT_PER_PIXEL=24, 
  BMP_BITMAP_HEADER_OFFSET = 40, 
  BMP_DEFAULT_PIXEL_PER_METER = 2835
};

void fill_bmp_header(struct bmp_header* p, uint64_t w, uint64_t h, uint64_t s);

int check_header(struct bmp_header* p);
/*  serializer   */
enum  write_status  {
  WRITE_OK = 0,
  WRITE_ERROR,
  WRITE_INVALID_BYTE
  /* коды других ошибок  */
};

enum write_status to_bmp( FILE* out, struct image const* img );
